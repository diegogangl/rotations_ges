# ## BEGIN GPL LICENSE BLOCK ##
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ## END GPL LICENSE BLOCK ##

"""
Convert JSX rotation to camera rotations in Blender

This script takes a JSX file exported from Google Earth Studio and converts
the camera rotations to Blender, adding cameras along the way.

"""

import os
import math
import re

import bpy

from bpy.types import Scene, Object
from typing import List, Dict


def remove_whitespace(text: str) -> str:
    """Remove all whitespace from string."""

    return ''.join(text.split())


def get_rotation_list(text: str, rotation: str) -> Dict:
    """Find and convert single rotation array."""

    if rotation == 'X':
        REGEX = r'cameraXRotation.setValuesAtTimes\(\[(.*?)\]\,\[(.*?)\]\);.'
    if rotation == 'Y':
        REGEX = r'cameraYRotation.setValuesAtTimes\(\[(.*?)\]\,\[(.*?)\]\);.'
    if rotation == 'Z':
        REGEX = r'cameraZRotation.setValuesAtTimes\(\[(.*?)\]\,\[(.*?)\]\);.'

    match = re.search(REGEX, text)

    if match:
        return {
            'times': [float(t) for t in match.group(1).split(',')],
            'values': [float(v) for v in match.group(2).split(',')],
        }


def parse_zoom(file_data: str) -> Dict:
    """Get zoom values and frames from jsx file."""

    REGEX = r'cameraZoom.setValueAtTime\((.*?),(.*?)\);'

    file_data = remove_whitespace(file_data)
    matches = re.findall(REGEX, file_data)

    times = []
    values = []

    for match in matches:
        times.append(float(match[0]))
        values.append(float(match[1]))

    return { 'times': times, 'values': values }


def parse_arrays(file_data: str) -> Dict:
    """Parse arrays from JSX file and convert them to lists."""

    file_data = remove_whitespace(file_data)

    return {
        'X': get_rotation_list(file_data, 'X'),
        'Y': get_rotation_list(file_data, 'Y'),
        'Z': get_rotation_list(file_data, 'Z'),
    }


def add_camera(scene: Scene) -> Object:
    """Add a new camera to the scene."""

    name = 'Ges Cam'
    cam = bpy.data.cameras.new(name)

    obj = bpy.data.objects.new(name, cam)
    scene.objects.link(obj)

    return obj


def secs_to_frame(secs: float) -> int:
    """Convert seconds to frame number."""

    fps = bpy.context.scene.render.fps
    return math.ceil(secs * fps)


def extend_animation(rotations: Dict):
    """Extend animation end to fit all keyframes."""

    current = bpy.context.scene.frame_end
    last_time = max(rotations['X']['times'])
    needed = secs_to_frame(last_time) + 1

    bpy.context.scene.frame_end = max(current, needed)


def apply_rotations(rotations: Dict, cam: Object):
    """Add rotations as keyframes to camera object."""

    for i, time in enumerate(rotations['X']['times']):

        # Convert seconds to frame
        frame = secs_to_frame(time)
        bpy.context.scene.frame_set(frame)

        # AE uses ZYX for rotations, while Blender uses XYZ
        # We need to add 90 degrees to X because X=0 on AE is "standing up"
        # on Blender. Finally we also discard Y because GES only has
        # support for tilt and pan

        rx = rotations['Z']['values'][i]
        rx = math.radians(rx)
        cam.rotation_euler.x = rx

        rz = rotations['X']['values'][i]
        rz = math.radians(rz + 90)
        cam.rotation_euler.z = rz

        # Add keyframe
        cam.keyframe_insert(data_path="rotation_euler", index=-1)


def main() -> None:
    """Entry point for the script."""

    filename = os.path.join('test_files', 'LX_global.jsx')

    with open(filename, 'r') as stream:
        rotations = parse_arrays(stream.read())
        cam = add_camera(bpy.context.scene)

        extend_animation(rotations)
        apply_rotations(rotations, cam)

    # Reset frame for easy testing
    bpy.context.scene.frame_set(0)

if __name__ == "__main__":
    main()
